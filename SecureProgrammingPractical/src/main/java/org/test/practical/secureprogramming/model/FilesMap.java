/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.test.practical.secureprogramming.model;
 import org.owasp.esapi.reference.RandomAccessReferenceMap; 
 import java.util.HashSet; 
 import java.util.Set; 
 import org.owasp.esapi.AccessReferenceMap;

/**
 *
 * @author Lubaba
 */
public class FilesMap {
    
    public static AccessReferenceMap getFilesMap(){ 
 
 Set fileSet = new HashSet();  fileSet.add("doc1.pdf");  
 fileSet.add("exampledoc.pdf");   
 return new RandomAccessReferenceMap(fileSet); 
    } 
   
    
}
