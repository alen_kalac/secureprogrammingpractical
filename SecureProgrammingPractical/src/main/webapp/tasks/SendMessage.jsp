<%@ include file="/header.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" isELIgnored="false" pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>

 <%@ page import="org.test.practical.secureprogramming.model.DBConnect"%>
 <%@taglib uri="../WEB-INF/csrfguard.tld" prefix="csrf" %>
 
<%
 if(session.getAttribute("isLoggedIn")!=null) 
 {
    if(request.getParameter("status")!=null)
    {
        out.print(request.getParameter("status")); //Displaying any error message
    }
    
%>
<br/><br/>

<csrf:form action="../SendMessage.do" method="POST">
<table> 
<tr><td>Recipient: </td><td><input type="text" name="recipient" value="<% if(request.getParameter("recipient")!=null){ out.print(request.getParameter("recipient")); } %>"/></tr></td>
<tr><td>Subject :</td><td><input type="text" name="subject"/></td></tr>
<tr><td>Message :</td><td><textarea name="msg"></textarea></td></tr>
<tr> <td><input type="hidden" name="sender" value="<%=session.getAttribute("user")%>"/></td></tr>
<tr><td><input type="submit" name="send" value="send"/></td></tr>
</table>  
</csrf:form>
<% 
 
 }
 else
 {
  out.print("<span style='color:red'>* Please login to send message</span>");   
 }
%>



 <%@ include file="/footer.jsp" %>