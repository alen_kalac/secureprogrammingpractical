 <%@page import="java.io.FileInputStream"%>
<%@page import="java.util.Properties"%>
<%@page import="java.io.File"%>
<%
   String path = request.getContextPath();
   String configPath=getServletContext().getRealPath("/WEB-INF/config.properties");
   
    Properties properties=new Properties();
    properties.load(new FileInputStream(configPath));
    String siteTitle=properties.getProperty("siteTitle");
     %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title><%=siteTitle%></title>
	<link rel="stylesheet" href="<%=path%>/style.css" type="text/css" charset="utf-8" />
           <% out.print("<script src=\""+path+"/jquery.min.js\" type=\"text/javascript\"></script>"); %>
</head>

<body>
<div id="container" >

     <div id="Menu">
		<ul id="menu-bar" style="margin-left: auto ;  margin-right: auto ;" >
			<li class="current"><a href="<%=path%>">Home</a></li>
			
			<li><a href="#">TASKS</a>
				<ul><li><a href="#">Task 1 - SQL Injection</a>
					
						<ul>
						  
						  <li><a href="<%=path%>/login.jsp">a. Bypass Login</a></li>
						  <li><a href="<%=path%>/tasks/sqli/download.jsp">b. Blind SQL Injection</a></li>
                                                  <li><a href="<%=path%>/tasks/forumposts.jsp?postid=1">c. Forum Posts</a></li>
                                                </ul>
                                         
                                     </li> 
			   	       <li><a href="#">Task 2 - XSS</a>
				             
					 	 <ul>
							<li> <a href="<%=path%>/tasks/xss/search.jsp">a. XSS 1</a></li>
                                                        <li><a href="<%=path%>/tasks/forum.jsp">b. XSS 2</a></li>
					       	 </ul>
                                              
						
					</li>
					<li><a href="#">Task 4 - Insecure Direct Object References</a>
					<ul>
                                            <li><a href="<%=path%>/myprofile.jsp?id=<% if(session.getAttribute("userid")!=null){ out.print(session.getAttribute("userid"));} %>" title="Make sure you have logged in ">a. IDOR 1</a>
						</li>
						<li><a href="<%=path%>/tasks/idor/download.jsp">b. IDOR 2</a>
						</li>
					</ul>
					</li>                                        
                                            <li><a href="#">Task 5 Unvalidated Redirect & Forward</a>
						<ul>
							<li><a href="<%=path%>/tasks/unvalidated/OpenForward.jsp">a. IRF</a>
							</li>
						</ul>
					</li> 
			</ul></li>
			<% 
                                if(session.getAttribute("isLoggedIn")!=null && session.getAttribute("isLoggedIn").equals("1"))
                                {
                                    if(session.getAttribute("privilege")!=null && session.getAttribute("privilege").equals("admin"))
                                    {
                                       out.print("<li><a href='"+path+"/admin/admin.jsp'>Admin Panel</a></li>"); 
                                    }
                                    out.print("<li><a href='"+path+"/myprofile.jsp?id="+session.getAttribute("userid")+"'>My Profile</a></li>");
                                     out.print("<li><a href='"+path+"/Logout'>Logout</a></li>");
                                }
                                else
                                {
                                   out.print("<li><a href='"+path+"/login.jsp'>Log in</a></li>");
                                }
                                %>
		</ul>
	</div>

	<div id="Main-Container">
	<br/>
	<div id="logo">

<h1><%=siteTitle%></h1>
</div>
<br/>
	
		
		<div id="Main">
		